# Ansible Role: node-lts

* Adds the Nodesource repo
* Installs nodejs and npm

## Requirements

  CentOS 7

## Role Variables

Path to the script that adds the repo
```
node_repo_script_path: https://rpm.nodesource.com/setup_8.x
```

## Dependencies

  None.

## Example Playbook
````
- hosts: localhost
  roles:
    - isleward.node-lts
````  

## Author Information

This role was created in 2018 by Vildravn.